<?php

interface iBatchOperation {
  public function getTotal();
  public function process($current, $total);
}

class BaseBatchOperation {
  static function execute(&$context, $step = 20) {
     $class = get_called_class();
     $operation = new $class($context, $step);
  }

  function __construct(&$context, $step) {
    $this->dbPrefix = variable_get('phpnuke_tableprefix', '').'_';
    $dbhost = variable_get('phpnuke_dbhost', NULL);
    $dbuser = variable_get('phpnuke_dbuser', NULL);
    $dbpass = variable_get('phpnuke_dbpass', NULL);
    $dbname = variable_get('phpnuke_dbname', NULL);
    $this->dbConnect($dbhost, $dbuser, $dbpass, $dbname);
    $this->context = &$context;
    $this->step = $step;
    // If it is the first time, initialize $context.
    if (empty($context['sandbox'])) {
      $context['sandbox']['total'] = $this->getTotal();
      $context['sandbox']['current'] = 0;
      $context['results']['sections'] = 0;
      if ($context['sandbox']['total'] == 0) {
        $context['finished'] = 1;
        return;
      }
    }

    // Execute the operation.
    $this->process($context['sandbox']['current'], $context['sandbox']['total']);
    
    // Update finished value 0..1
    $context['finished'] = $context['sandbox']['current'] / $context['sandbox']['total'];
  }
  
  /**
   * Update the $context upon each iteration of this operation.
   *
   * This function is to be called at the end of each iteration in the operation loop.
   */
  function updateContext() {
    $this->context['results'][$this->token]++;
    $this->context['sandbox']['current']++;
    $current = $context['sandbox']['current'];
    $this->context['message'] = t('Importing @token  @current of @total.', array(
      '@token' => $this->token,
      '@current' => $this->context['sandbox']['current'],
      '@total' => $this->context['sandbox']['total']
      ));
  }

  function dbConnect($dbhost, $dbuser, $dbpass, $dbname) {
    $link = @mysql_connect($dbhost, $dbuser, $dbpass, TRUE, 2);
    if (!$link) {
      drupal_set_message(t('Unable to connect to database at «%host» with user «%user» and password «%pass».', array('%host' => $dbhost, '%user' => $dbuser, '%pass' => $dbpass)), 'error');
    }
    elseif (!mysql_select_db($dbname)) {
        drupal_set_message(t('Unable to select database :db.', array(':db' => $dbname)), 'error');
        $link = FALSE;
    }
    if (!$link) {
      form_set_error('', t("Unable to stablish a connection to PHP-Nuke database. Review the settings in the <a href='@url'>database configuration page</a>.", array('@url'=>url('admin/settings/phpnuke/database'))));
    }
    mysql_query('SET NAMES "utf8"', $link);
    $this->link = $link;
  }
  
  function dbQuery($query) {
    $args = func_get_args();
    array_shift($args);
    if (isset($args[0]) and is_array($args[0])) { // 'All arguments in one array' syntax
      $args = $args[0];
    }
    _db_query_callback($args, TRUE);
    $query = preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $query);
    $query = strtr($query, array('{' => $this->dbPrefix, '}' => ''));
    $result = mysql_query($query, $this->link);
    
    if (!mysql_errno($this->link)) {
      return $result;
    }
    else {
      // Indicate to drupal_error_handler that this is a database error.
      ${DB_ERROR} = TRUE;
      trigger_error(check_plain(mysql_error($this->link) ."\nquery: ". $query), E_USER_WARNING);
      return FALSE;
    }
  }
}
