<?php

require_once dirname(__FILE__).'/../base.batch.inc';

/**
 * Import authors (admins)
 */
class phpNukeImportAuthors extends BaseBatchOperation implements iBatchOperation {
  public $token = 'authors';

  function getTotal() {
    $query = 'SELECT COUNT(aid) FROM {authors}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }

  function process($current, $total) {
    $query = "SELECT * FROM {authors} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $account = array(
        'name' => $row->aid,
        'mail' => $row->email,
        'password' => $row->pwd,
        'status' => 1,
        'roles' => $this->processRoles($row)
      );
      $account = user_save(NULL, $account);
      $record = array('aid' => $row->aid, 'uid' => $account->uid);
      drupal_write_record('phpnuke_authors', $record);
      $this->updateContext();
    }
  }
  
  function processRoles($row) {
    static $roles_map = NULL;

    if (is_null($roles_map)) {
      $phpnuke_roles = phpnuke_roles();
      foreach ($phpnuke_roles as $rol) {
        $roles_map[$rol] = variable_get('phpnuke_rol_'.$rol, 0);
      }
    }
    
    $roles = array();
    foreach ($roles_map as $rol => $rid) {
      if ($row->$rol) {
        $roles[] = $roles_map[$rol];
      }
    }

    return $roles;
  }  
}
