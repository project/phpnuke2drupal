<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportComments extends BaseBatchOperation implements iBatchOperation {
  public $token = 'comments';

  function getTotal() {
    $query = 'SELECT COUNT(tid) FROM {comments}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $stories = array();
    $comments = array();
    $query = "SELECT * FROM {comments} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      //Related story.
      if (!isset($stories[$row->sid])) {
        $stories[$row->sid] = db_result(db_query('SELECT nid FROM {phpnuke_stories} WHERE sid=%d',$row->sid));
      }
      //Parent.
      $pcid = 0;
      if ($row->pid != 0) {
        if (!isset($comments[$row->pid])) {
          $comments[$row->pid] = db_result(db_query('SELECT cid FROM {phpnuke_comments} WHERE tid=%d', $row->pid));
        }
        $pcid = $comments[$row->pid];
      }
      // Save the comment.
      $comment = array(
        'uid'      => 0, // TODO. can obtain a username based on $row->name or $row->mail
        'name'     => $row->name,
        'mail'     => $row->email,
        'homepage' => $row->url,
        'hostname' => $data->host_name,
        'nid'      => $stories[$row->sid],
        'subject'  => $row->subject,
        'comment'  => _filter_htmlcorrector($row->comment),
        'format'   => 1, // unfiltered HTML
        'pid'      => $pcid,
        'status'   => $row->reason
      );
      $cid = comment_save($comment);
      // sadly the comment_save function sets timestamp to time()...
      db_query('UPDATE {comments} SET timestamp=%d WHERE cid=%d', array(strtotime($row->date), $cid));
      $record = array('tid' => $row->tid, 'cid' => $cid);
      drupal_write_record('phpnuke_comments', $record);
      $this->updateContext();
    }
  }
}
