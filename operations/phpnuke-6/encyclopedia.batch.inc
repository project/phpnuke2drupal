<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportEncyclopedia extends BaseBatchOperation implements iBatchOperation {
  public $token = 'encyclopedia';

  function getTotal() {
    $query = "SELECT COUNT(eid) FROM {encyclopedia} WHERE active=1";
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $vid = variable_get('phpnuke2drupal_vocabulary_'.$this->token, 0);
    $query = "SELECT * FROM {encyclopedia} WHERE active=1 LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $term = array(
        'vid' => $vid,
        'name' => $row->title,
        'description' => $row->description
      );
      taxonomy_save_term($term);
      $record = array('eid' => $row->eid, 'tid' => $term['tid']);
      drupal_write_record('phpnuke_encyclopedia', $record);
      $this->updateContext();
    }
  }
}
