<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportEncyclopediaText extends BaseBatchOperation implements iBatchOperation {
  public $token = 'encyclopedia_text';

  function getTotal() {
    $query = 'SELECT COUNT(tid) FROM {encyclopedia_text} et INNER JOIN {encyclopedia} e ON e.eid = et.eid WHERE e.active=1';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $type = variable_get('phpnuke2drupal_node_type_'.$this->token, 0);
    $sections = array();
    $query = "SELECT * FROM {encyclopedia_text} et INNER JOIN {encyclopedia} e ON e.eid = et.eid WHERE e.active=1 LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $node = (object)array(
        'type' => $type,
        'title' => $row->title,
        'body' => _filter_htmlcorrector($row->text),
        'comment' => 0, // (0 = no comments)
        'format' => 2, // Full HTML
        'status' => 1,
        'uid' => 1
      );
      node_save($node);
      if ($node->nid) {
        $record = array('tid' => $row->tid, 'nid' => $node->nid);
        drupal_write_record('phpnuke_encyclopedia_text', $record);
        // Taxonomy term for section.
        if (!isset($sections[$row->eid])) {
          $tid = db_result(db_query('SELECT tid from {phpnuke_encyclopedia} WHERE eid = '.$row->eid));
          $sections[$row->eid] = $tid;
        }
        $term = $sections[$row->eid];
        taxonomy_node_save($node, array($term));
      }
      $this->updateContext();
    }
  }
}
