<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportLinksCategories extends BaseBatchOperation implements iBatchOperation {
  public $token = 'links_categories';

  function getTotal() {
    $query = 'SELECT COUNT(cid) FROM {links_categories}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    // All categories will be cached. We need this to set parents properly in a second step.
    $cats = array();

    $vid = variable_get('phpnuke2drupal_vocabulary_'.$this->token, 0);
    $query = "SELECT * FROM {links_categories} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $term = array(
        'vid' => $vid,
        'name' => $row->title,
        'description' => $row->cdescription
      );
      taxonomy_save_term($term);
      $record = array('cid' => $row->cid, 'tid' => $term['tid']);
      $cats[$row->cid] = array('term' => $term, 'row' => $row);
      drupal_write_record('phpnuke_links_categories', $record);
      $this->updateContext();
    }
    
    // Set parents.
    foreach ($cats as $cat) {
      $row = $cat['row'];
      if ($row->parentid != '0') {
        $term = $cat['term'];
        $parent = $cat[$cat->parentid]['term'];
        $term['parent'] = $parent['tid'];
        taxonomy_save_term($term);
      }
    }
  }
}
