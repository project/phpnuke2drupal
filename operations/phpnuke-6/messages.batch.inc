<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportMessages extends BaseBatchOperation implements iBatchOperation {
  public $token = 'messages';

  function getTotal() {
    $query = 'SELECT COUNT(mid) FROM {message}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $type = variable_get('phpnuke2drupal_node_type_'.$this->token, 0);
    $categories = array();
    $topics = array();
    $query = "SELECT * FROM {message} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $node = (object)array(
        'type' => $type,
        'created' => strtotime($row->date),
        'title' => $row->title,
        'body' => _filter_htmlcorrector($row->content),
        'comment' => 0, // (0 = no comments)
        'format' => 2, // Full HTML
        'status' => $row->active,
        'uid' => 1 // TODO: columnas reviewer / email ¿?
      );
      node_save($node);
      if ($node->nid) {
        $record = array('mid' => $row->mid, 'nid' => $node->nid);
        drupal_write_record('phpnuke_messages', $record);
      }
      $this->updateContext();
    }
  }
}
