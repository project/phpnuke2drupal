<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportReviews extends BaseBatchOperation implements iBatchOperation {
  public $token = 'reviews';

  function getTotal() {
    $query = 'SELECT COUNT(id) FROM {reviews}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $type = variable_get('phpnuke2drupal_node_type_'.$this->token, 0);
    $categories = array();
    $topics = array();
    $query = "SELECT * FROM {reviews} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $node = (object)array(
        'type' => $type,
        'created' => strtotime($row->date),
        'title' => $row->title,
        'body' => _filter_htmlcorrector($row->text),
        'comment' => 1, // (1 = no further comments)
        'format' => 2, // Full HTML
        'status' => 1,
        'uid' => 1 // TODO: columnas reviewer / email ¿?
      );
      node_save($node);
      if ($node->nid) {
        $record = array('id' => $row->id, 'nid' => $node->nid);
        drupal_write_record('phpnuke_reviews', $record);
      }
      $this->updateContext();
    }
  }
}
