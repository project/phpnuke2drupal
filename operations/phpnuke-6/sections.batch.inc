<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportSections extends BaseBatchOperation implements iBatchOperation {
  public $token = 'sections';

  function getTotal() {
    $query = 'SELECT COUNT(secid) FROM {sections}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $vid = variable_get('phpnuke2drupal_vocabulary_'.$this->token, 0);
    $query = "SELECT * FROM {sections} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $term = array(
        'vid' => $vid,
        'name' => $row->secname
      );
      taxonomy_save_term($term);

      // Store the mapping.
      $record = array('secid' => $row->secid, 'tid' => $term['tid']);

      $this->updateContext();
    }
  }
}
