<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportStories extends BaseBatchOperation implements iBatchOperation {
  public $token = 'stories';

  function getTotal() {
    $query = 'SELECT COUNT(sid) FROM {stories}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $type = variable_get('phpnuke2drupal_node_type_'.$this->token, 0);
    $categories = array();
    $topics = array();
    $query = "SELECT * FROM {stories} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $teaser = _filter_htmlcorrector($row->hometext);
      $node = (object)array(
        'type' => $type,
        'created' => strtotime($row->time),
        'title' => $row->title,
        'teaser' => $teaser,
        'body' => $teaser.' '._filter_htmlcorrector($row->bodytext),
        'comment' => 1, // (1 = no further comments)  // TODO la tabla nuke tiene valores 0,1¿?
        'format' => 2, // Full HTML
        'status' => 1,
        'uid' => 1 // TODO: columnas aid / informant ¿?
      );
      if ($row->has_media) {
        $node->files = $this->importFiles($row->sid);
      }
      node_save($node);
      if ($node->nid) {
        $record = array('sid' => $row->sid, 'nid' => $node->nid);
        drupal_write_record('phpnuke_stories', $record);
        // Taxonomy term for category.
        if (!isset($categories[$row->catid])) {
          $tid = db_result(db_query('SELECT tid from {phpnuke_stories_categories} WHERE catid = '.$row->catid));
          $categories[$row->catid] = $tid;
        }
        $term = $categories[$row->catid];
        taxonomy_node_save($node, array($term));
        // Taxonomy term for topic.
        if (!isset($topics[$row->topic])) {
          $tid = db_result(db_query('SELECT tid from {phpnuke_topics} WHERE topicid = '.$row->topic));
          $topics[$row->topic] = $tid;
        }
        $term = $topics[$row->topic];
        taxonomy_node_save($node, array($term));
      }
      $this->updateContext();
    }
  }
  
  function importFiles($sid) {}
}
