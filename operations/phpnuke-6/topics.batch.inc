<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportTopics extends BaseBatchOperation implements iBatchOperation {
  public $token = 'topics';

  function getTotal() {
    $query = "SELECT COUNT(topicid) FROM {topics}";
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $vid = variable_get('phpnuke2drupal_vocabulary_'.$this->token, 0);
    $query = "SELECT * FROM {topics} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $term = array(
        'vid' => $vid,
        'name' => $row->topicname,
        'description' => $row->topictext,
      );
      taxonomy_save_term($term);
      $record = array('topicid' => $row->topicid, 'tid' => $term['tid']);
      drupal_write_record('phpnuke_topics', $record);
      $this->updateContext();
    }
  }
}
