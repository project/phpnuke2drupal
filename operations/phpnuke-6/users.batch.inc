<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportUsers extends BaseBatchOperation implements iBatchOperation {
  public $token = 'users';

  function getTotal() {
    $query = 'SELECT COUNT(uid) FROM {users}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $query = "SELECT * FROM {users} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $account = array(
        'name' => $row->uname,
        'mail' => $row->email,
        'created' => strtotime($row->user_regdate),
        'password' => $row->pass,
        'status' => 1
      );
      $account = user_save(NULL, $account);
      $record = array('nuke_uid' => $row->uid, 'uid' => $account->uid);
      drupal_write_record('phpnuke_users', $record);
      $this->updateContext();
    }
  }
}
