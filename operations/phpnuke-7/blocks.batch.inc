<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportBlocks extends BaseBatchOperation implements iBatchOperation {
  public $token = 'blocks';

  function getTotal() {
    $query = 'SELECT COUNT(lid) FROM {blocks} WHERE blockfile = \'\'';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $query = "SELECT * FROM {blocks} WHERE blockfile = '' LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $block = array(
        'title' => $row->title,
        'body' => _filter_htmlcorrector($row->content),
      );
      $bid = $this->addBlock($block);
      $record = array('blockid' => $row->bid, 'bid' => $bid);
      drupal_write_record('phpnuke_blocks', $record);
      $this->updateContext();
    }
  }
  
  function addBlock($block) {
    db_query("INSERT INTO {boxes} (body, info, format) VALUES ('%s', '%s', %d)", $block->body, $block->info, $block->format);
    $delta = db_last_insert_id('boxes', 'bid');

    foreach (list_themes() as $key => $theme) {
      if ($theme->status) {
        db_query("INSERT INTO {blocks} (visibility, pages, custom, title, module, theme, status, weight, delta, cache) VALUES(%d, '%s', %d, '%s', '%s', '%s', %d, %d, '%s', %d)", $form_state['values']['visibility'], trim($form_state['values']['pages']), $form_state['values']['custom'], $form_state['values']['title'], $form_state['values']['module'], $theme->name, 0, 0, $delta, BLOCK_NO_CACHE);
      }
    }
    
    return $delta;
  }
}
