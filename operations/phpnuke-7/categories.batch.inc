<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportCategories extends BaseBatchOperation implements iBatchOperation {
  public $token = 'catagories';

  function getTotal() {
    $query = "SELECT COUNT(cat_id) FROM {catagories}";
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $vid = variable_get('phpnuke2drupal_vocabulary_'.$this->token, 0);
    $query = "SELECT * FROM {catagories} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $term = array(
        'vid' => $vid,
        'name' => $row->cat_title
      );
      taxonomy_save_term($term);
      $record = array('catid' => $row->cat_id, 'tid' => $term['tid']);
      drupal_write_record('phpnuke_categories', $record);
      $this->updateContext();
    }
  }
}
