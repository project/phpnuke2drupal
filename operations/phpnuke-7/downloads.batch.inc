<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportDownloads extends BaseBatchOperation implements iBatchOperation {
  public $token = 'downloads';

  function getTotal() {
    $query = 'SELECT COUNT(lid) FROM {downloads_downloads}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $categories = array();
    $type = variable_get('phpnuke2drupal_node_type_'.$this->token, 0);
    $query = "SELECT * FROM {downloads_downloads} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
    // TODO: sid?
      $timestamp = strtotime($row->date);
      $files = $this->importFile($row->url, $timestamp);
      $node = (object)array(
        'type'    => $type,
        'created' => $timestamp,
        'title'   => $row->title,
        'body'    => _filter_htmlcorrector($row->description),
        'comment' => 0, // (0 = no comments)
        'format'  => 2, // Full HTML
        'status'  => 1,
        'uid'     => 1, // TODO: columnas name y email
        'files'   => $files,
      );

      node_save($node);
      if ($node->nid) {
        $record = array('lid' => $row->lid, 'nid' => $node->nid);
        drupal_write_record('phpnuke_downloads_downloads', $record);
        // Taxonomy term for category.
        if (!isset($categories[$row->cid])) {
          $tid = db_result(db_query('SELECT tid from {phpnuke_downloads_categories} WHERE cid = '.$row->cid));
          $categories[$row->cid] = $tid;
        }
        $term = $categories[$row->cid];
        taxonomy_node_save($node, array($term));
      }
      $this->updateContext();
    }
  }
  
  function importFile($file_url, $timestamp) {
    $url = variable_get('phpnuke_url', NULL);
    $path = variable_get('phpnuke_path', NULL);
    
    $src = $path.'/'.substr($file_url, strlen($url));
    if (!file_copy($src)) {
      form_set_error('', t('«%file» not found.', array('%file' => $src)));
      return;
    }
    $filename = array_pop(explode('/', $file_url));
    $filemime = file_get_mimetype($src);
    $files_path = file_directory_path();
    $file = (object)array(
      'uid'       => 1,
      'filename'  => $filename,
      'filepath'  => $files_path.'/'.$filename,
      'filemime'  => $filemime,
      'filesize'  => $data->taille,
      'status'    => FILE_STATUS_TEMPORARY,
      'timestamp' => $timestamp,
    );
    drupal_write_record('files', $file);

    $file->new = TRUE;
    $file->list = TRUE;
    $file->description = $data->titre;
    $files = array();
    $files[$file->fid] = $file;

    return $files;
  }
}
