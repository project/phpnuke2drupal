<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportEvents extends BaseBatchOperation implements iBatchOperation {
  public $token = 'events';

  function getTotal() {
    $query = 'SELECT COUNT(eid) FROM {events}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $timezone = variable_get('date_default_timezone_id', 0);
    $type = variable_get('phpnuke2drupal_node_type_'.$this->token, 0);
    $query = "SELECT * FROM {events} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $node = (object)array(
        'type' => $type,
        'created' => strtotime($row->posteddate),
        'title' => $row->title,
        'body' => _filter_htmlcorrector($row->hometext),
        'comment' => 1, // (1 = no further comments)
        'format' => 2, // Full HTML
        'status' => $row->activ,
        'uid' => 1, // TODO
        'event' => array(
          'event_start'  => event_explode_date($row->startDate.' '.$row->startTime),
          'event_end'    => event_explode_date($row->endDate.' '.$row->endTime),
          'timezone'     => $timezone,
          'has_time'     => 1,
          'has_end_date' => !$row->alldayevent,
          'start_in_dst' => 1,
          'end_in_dst'   => 1,
        )
      );

      node_save($node);
      if ($node->nid) {
        $record = array('eid' => $row->eid, 'nid' => $node->nid);
        drupal_write_record('phpnuke_events', $record);
      }

      $this->updateContext();
    }
  }
}
