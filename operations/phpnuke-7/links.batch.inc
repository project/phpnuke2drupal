<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportlinks extends BaseBatchOperation implements iBatchOperation {
  public $token = 'links';

  function getTotal() {
    $query = 'SELECT COUNT(lid) FROM {links_links}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $categories = array();
    $type = variable_get('phpnuke2drupal_node_type_'.$this->token, 0);
    $query = "SELECT * FROM {links_links} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $node = (object)array(
        'type'    => $type,
        'created' => strtotime($row->date),
        'title'   => $row->title,
        'body'    => _filter_htmlcorrector($row->description),
        'url'     => $row->url,
        'comment' => 1, // (1 = no further comments)
        'format'  => 2, // Full HTML
        'status'  => 1,
        'uid'     => 1 // TODO
      );
      node_save($node);
      if ($node->nid) {
        $record = array('lid' => $row->lid, 'nid' => $node->nid);
        drupal_write_record('phpnuke_links_links', $record);
        // Taxonomy term for category.
        if (!isset($categories[$row->cid])) {
          $tid = db_result(db_query('SELECT tid from {phpnuke_links_categories} WHERE cid = '.$row->cid));
          $categories[$row->cid] = $tid;
        }
        $term = $categories[$row->cid];
        taxonomy_node_save($node, array($term));
      }
      $this->updateContext();
    }
  }
}
