<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportSectionsContent extends BaseBatchOperation implements iBatchOperation {
  public $token = 'sections_content';

  function getTotal() {
    $query = 'SELECT COUNT(artid) FROM {seccont}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $type = variable_get('phpnuke2drupal_node_type_'.$this->token, 0);
    $sections = array();
    $query = "SELECT * FROM {seccont} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $node = (object)array(
        'type' => $type,
        'title' => $row->title,
        'body' => _filter_htmlcorrector($row->content),
        'comment' => 0, // (0 = no comments)
        'format' => 2, // Full HTML
        'status' => 1,
        'uid' => 1
      );
      node_save($node);
      if ($node->nid) {
        $record = array('artid' => $row->sid, 'nid' => $node->nid);
        drupal_write_record('phpnuke_sections_content', $record);
        // Taxonomy term for section.
        if (!isset($sections[$row->secid])) {
          $tid = db_result(db_query('SELECT tid from {phpnuke_sections} WHERE secid = '.$row->secid));
          $sections[$row->secid] = $tid;
        }
        $term = $sections[$row->secid];
        taxonomy_node_save($node, array($term));
      }
      $this->updateContext();
    }
  }
}
