<?php

require_once dirname(__FILE__).'/../base.batch.inc';

class phpNukeImportStoriesCategories extends BaseBatchOperation implements iBatchOperation {
  public $token = 'stories_categories';

  function getTotal() {
    $query = 'SELECT COUNT(catid) FROM {stories_cat}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $vid = variable_get('phpnuke2drupal_vocabulary_'.$this->token, 0);
    $query = "SELECT * FROM {stories_cat} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $term = array(
        'vid' => $vid,
        'name' => $row->title
      );
      taxonomy_save_term($term);
      $record = array('catid' => $row->catid, 'tid' => $term['tid']);
      drupal_write_record('phpnuke_stories_categories', $record);
      $this->updateContext();
    }
  }
}
