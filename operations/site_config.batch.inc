<?php

require_once 'base.batch.inc';

class phpNukeImportSiteConfig extends BaseBatchOperation implements iBatchOperation {
  public $token = 'config';

  function getTotal() {
    return 1;
  }

  function process($current, $total) {
    $query = "SELECT * FROM {config} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    $row = mysql_fetch_object($rsc);
    // frontpage
    /*
    $query = 'SELECT main_module FROM {main}';
    $main = db_result($this->dbQuery($query));
    $frontpage = $main; // this will be a view
    variable_set('site_frontpage', $frontname);
    */
    
    // install time
    //variable_set('install_time', strtotime($row->startdate));

    // Languages.
/* Let's be permissive. I haven't seen a difference in multilingual=1 sites.
    if ($row->multilingual != 0) {
      form_set_error('', t('This module does not support multilingual setups. Ask in the issue queue.'));
    }
*/
    // Identify the language code.
    $langcode = NULL;
    require_once 'includes/locale.inc';
    foreach (_locale_get_predefined_list() as $code => $lang) {
      if (strtolower($lang[0]) == $row->language) {
        $langcode = $code;
        break;
      }
    }
    if (is_null($langcode)) {
      form_set_error('', t('Unknown language @language.', array('@language' => $row->language)));
    }
    // If language is not set up, do it.
    elseif (!array_key_exists($langcode, language_list())) {
      locale_add_language($langcode);
      if ($batch = locale_batch_by_language($langcode)) {
        batch_set($batch);
      }
    }

    // site-information.
    variable_set('site_name', $row->sitename);
    variable_set('site_slogan', $row->slogan);
    variable_set('site_mail', $row->adminmail);
    variable_set('site_footer', $row->foot1 + $row->foot2 + $row->foot3);

    // permissions
//    $row->anonpost

/*
    $row->site_logo
    $row->pollcomm
    $row->articlecomm
    $row->storyhome
    $row->user_news
    $row->banners
    $row->backend_title
    $row->backend_language
    $row->notify_email
*/
    $this->updateContext();
  }
}
