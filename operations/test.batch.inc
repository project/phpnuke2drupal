<?php

require_once dirname(__FILE__).'/../base.batch.inc';

// If this is a phpnuke-version specific file use this instead.
//require_once dirname(__FILE__).'/../base.batch.inc';

/**
 * Example batch operation.
 */
class phpNukeImportTest extends BaseBatchOperation implements iBatchOperation {
  public $token = 'test';

  function getTotal() {
    $query = 'SELECT COUNT(test) FROM {test}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $query = "SELECT * FROM {test} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      // do something

      $this->updateContext();
    }
  }
}
